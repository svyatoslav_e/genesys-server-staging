/**
 * Copyright 2015 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import org.genesys2.util.NumberUtils;
import org.junit.Test;

public class NumberUtilsTest {
	@Test
	public void test1() {
		assertThat(NumberUtils.numericValue(null), is(0f));
		assertThat(NumberUtils.numericValue(""), is(0f));
		assertThat(NumberUtils.numericValue("    "), is(0f));
		assertThat(NumberUtils.numericValue("bbbb"), is(0f));
		assertThat(NumberUtils.numericValue("bb aa"), is(0f));
		assertThat(NumberUtils.numericValue("bb aa"), is(0f));

		assertThat(NumberUtils.numericValue("01"), is(1f));
		assertThat(NumberUtils.numericValue(" 1 x"), is(1f));
		assertThat(NumberUtils.numericValue("y 1 x"), is(1f));

		assertThat(NumberUtils.numericValue("PFX 1"), is(1f));
		assertThat(NumberUtils.numericValue("PFX 1.1"), is(1.1f));
		assertThat(NumberUtils.numericValue("PFX 1a"), is(1f));
		assertThat(NumberUtils.numericValue("PFX 1.b1"), is(1.1f));

		assertThat(NumberUtils.numericValue("1.1"), is(1.1f));
		assertThat(NumberUtils.numericValue("1.01"), is(1.01f));
		assertThat(NumberUtils.numericValue("1.001"), is(1.001f));
		assertThat(NumberUtils.numericValue("1.0001"), is(1.0001f));
		assertThat(NumberUtils.numericValue("1.0009"), is(1.0009f));
		assertThat(NumberUtils.numericValue("1.00099"), is(1.00099f));
		assertThat(NumberUtils.numericValue("1.000999"), is(1.000999f));

		assertThat(NumberUtils.numericValue("" + Integer.MAX_VALUE), is((float) Integer.MAX_VALUE));

		assertThat(NumberUtils.numericValue("-10"), is(10f));
		assertThat(NumberUtils.numericValue("a-10"), is(10f));

		assertThat(NumberUtils.numericValue("CIP 102081.28"), is(102081.28f));
		assertThat(NumberUtils.numericValue("CIP 102081.29"), is(102081.29f));
		assertThat(NumberUtils.numericValue("TMe-419"), is(419f));
		assertThat(NumberUtils.numericValue("09H6400061"), is(9.6400061f));
	}
}
