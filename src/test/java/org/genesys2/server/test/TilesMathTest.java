/**
 * Copyright 2014 Global Crop Diversity Trust
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.test;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.*;

import static org.genesys2.server.service.MappingService.CoordUtil.latToImg;
import static org.genesys2.server.service.MappingService.CoordUtil.lonToImg;
import static org.genesys2.server.service.MappingService.CoordUtil.latToImg3;
import static org.genesys2.server.service.MappingService.CoordUtil.lonToImg3;
import static org.genesys2.server.service.MappingService.CoordUtil.tileToLat;
import static org.genesys2.server.service.MappingService.CoordUtil.tileToLon;

import org.genesys2.server.service.MappingService.CoordUtil;
import org.junit.Test;

public class TilesMathTest {

	@Test
	public void test1() {
		assertEquals(-180.0, tileToLon(1, 0), 0);
		assertEquals(0.0, tileToLon(1, 1), 0);
		assertEquals(180.0, tileToLon(1, 2), 0);

		assertEquals(85.0, tileToLat(1, 0), 0.1);
		assertEquals(0.0, tileToLat(1, 1), 0.1);
		assertEquals(-85, tileToLat(1, 2), 0.1);

		assertEquals(0, tileToLon(2, 2), 0);
		assertEquals(90, tileToLon(2, 3), 0);
		assertEquals(180, tileToLon(2, 4), 0);

		for (int zoom = 0; zoom < 20; zoom++) {
			double last = -200.0;
			for (int tile = 0; tile <= (1 << zoom); tile++) {
				double curr = tileToLon(zoom, tile);
				assertThat("Zoom=" + zoom + " tile=" + tile + " problem", curr, greaterThan(last));
				last = curr;
			}
		}
	}

	@Test
	public void testLonToImg() {
		assertEquals(0, lonToImg(1, -180.0));
		assertEquals(0, lonToImg(2, -180.0));
		assertEquals(0, lonToImg(3, -180.0));
		assertEquals(0, lonToImg(4, -180.0));
		assertEquals(0, lonToImg(5, -180.0));

		assertEquals(255, lonToImg(1, 179.99999));
		assertEquals(255, lonToImg(2, 179.99999));
		assertEquals(255, lonToImg(3, 179.99999));
		assertEquals(255, lonToImg(4, 179.99999));
		assertEquals(255, lonToImg(5, 179.99999));
	}

	/**
	 * https://www.genesys-pgr.org/acn/id/4620209 doesn't show at zoom level 9
	 */
	@Test
	public void acn4620209() {
		double lat = 33.07361, lon = -16.32389;
		assertEquals(200, lonToImg3(9, 232, lon));
		assertEquals(145, lonToImg3(10, 465, lon));
		assertThat(tileToLon(8, 116), lessThanOrEqualTo(lon));
		assertThat(tileToLon(8, 116 + 1), greaterThanOrEqualTo(lon));
		assertThat(tileToLon(9, 232), lessThanOrEqualTo(lon));
		assertThat(tileToLon(9, 232 + 1), greaterThanOrEqualTo(lon));

		assertEquals(103, latToImg3(0, 0, lat));
		assertEquals(156, latToImg3(2, 1, lat));
		assertEquals(56, latToImg3(3, 3, lat));
		assertEquals(112, latToImg3(4, 6, lat));
		assertEquals(225, latToImg3(5, 12, lat));
		assertEquals(195, latToImg3(6, 25, lat));
		assertEquals(134, latToImg3(7, 51, lat));

		assertEquals(13, latToImg3(8, 103, lat));
		assertThat(CoordUtil.tileToLat(8, 103), greaterThanOrEqualTo(lat));
		assertThat(CoordUtil.tileToLat(8, 103 + 1), lessThanOrEqualTo(lat));

		assertEquals(27, latToImg3(9, 206, lat));
		assertThat(CoordUtil.tileToLat(9, 206), greaterThanOrEqualTo(lat));
		assertThat(CoordUtil.tileToLat(9, 206 + 1), lessThanOrEqualTo(lat));

		assertEquals(55, latToImg3(10, 412, lat));
		assertThat(CoordUtil.tileToLat(10, 412), greaterThanOrEqualTo(lat));
		assertThat(CoordUtil.tileToLat(10, 412 + 1), lessThanOrEqualTo(lat));

		assertEquals(111, latToImg3(11, 824, lat));
	}

	@Test
	public void testLonToImg3() {
		assertEquals(0, lonToImg3(1, 0, -180.0));
		assertEquals(0, lonToImg3(2, 0, -180.0));
		assertEquals(0, lonToImg3(3, 0, -180.0));
		assertEquals(0, lonToImg3(4, 0, -180.0));
		assertEquals(0, lonToImg3(9, 0, -180.0));

		assertEquals(255, lonToImg3(1, 1, 179.99999));
		assertEquals(255, lonToImg3(2, (1 << 2) - 1, 179.99999));
		assertEquals(255, lonToImg3(3, (1 << 3) - 1, 179.99999));
		assertEquals(255, lonToImg3(4, (1 << 4) - 1, 179.99999));
		assertEquals(255, lonToImg3(9, (1 << 9) - 1, 179.99999));
		assertEquals(255, lonToImg3(12, (1 << 12) - 1, 179.99999));
		assertEquals(255, lonToImg3(20, (1 << 20) - 1, 179.9999999999));
	}

	@Test
	public void testLatToImg3() {
		assertEquals(0, latToImg(1, 85.0511287));
		assertEquals(0, latToImg3(1, 0, 85.0));
		assertEquals(0, latToImg3(2, 0, 85.05));
		assertEquals(0, latToImg3(3, 0, 85.05));
		assertEquals(0, latToImg3(4, 0, 85.05));
		assertEquals(0, latToImg3(9, 0, 85.051));
		assertEquals(0, latToImg3(20, 0, 90));

		assertEquals(128, latToImg3(0, 0, 0.0));
		assertEquals(256, latToImg3(1, 0, 0.0));
		assertEquals(0, latToImg3(2, (1 << 2) / 2, 0.0));
		assertEquals(0, latToImg3(3, (1 << 3) / 2, 0.0));
		assertEquals(0, latToImg3(4, (1 << 4) / 2, 0.0));
		assertEquals(0, latToImg3(9, (1 << 9) / 2, 0.0));

		assertEquals(255, latToImg3(1, 1, -85.051));
		assertEquals(255, latToImg3(2, (1 << 2) - 1, -85.051));
		assertEquals(255, latToImg3(3, (1 << 3) - 1, -85.051));
		assertEquals(255, latToImg3(4, (1 << 4) - 1, -85.051));
		assertEquals(255, latToImg3(9, (1 << 9) - 1, -85.051));
		assertEquals(255, latToImg3(12, (1 << 12) - 1, -85.0511));
		assertEquals(255, latToImg3(20, (1 << 20) - 1, -85.0511287));
	}

}
