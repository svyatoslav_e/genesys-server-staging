package org.genesys2.tests.resttests.docs;

import static org.hamcrest.Matchers.*;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.*;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.*;
import static org.springframework.restdocs.payload.PayloadDocumentation.*;
import static org.springframework.restdocs.request.RequestDocumentation.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.util.ArrayList;
import java.util.Collection;
import java.util.UUID;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.genesys2.server.model.genesys.MaterialRequest;
import org.genesys2.server.model.genesys.MaterialSubRequest;
import org.genesys2.server.model.impl.FaoInstitute;
import org.genesys2.tests.resttests.AbstractRestTest;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.restdocs.RestDocumentation;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;

@Transactional(transactionManager = "transactionManager")
public class ApiRequestsDocsTest extends AbstractRestTest {

	private static final String TEST_INSTCODE = "XXX001";

	private static final Log LOG = LogFactory.getLog(ApiRequestsDocsTest.class);

	@Rule
	public final RestDocumentation restDocumentation = new RestDocumentation("target/generated-snippets");

	@Autowired
	WebApplicationContext webApplicationContext;

	MockMvc mockMvc;
	private static final ObjectMapper objectMapper;

	private FaoInstitute institute;

	private UUID requestUuid;

	static {
		objectMapper = new ObjectMapper();
		objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		objectMapper.configure(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);
		objectMapper.configure(SerializationFeature.WRITE_EMPTY_JSON_ARRAYS, false);
		objectMapper.setSerializationInclusion(Include.NON_EMPTY);
	}

	@Before
	public void setUp() {
		mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext)
				.apply(documentationConfiguration(this.restDocumentation).uris().withScheme("https").withHost("sandbox.genesys-pgr.org").withPort(443)).build();

		Collection<FaoInstitute> institutes = new ArrayList<>();
		institute = new FaoInstitute();
		institute.setCode(TEST_INSTCODE);
		institutes.add(institute);
		instituteService.update(institutes);

		MaterialRequest request = new MaterialRequest();
		request.setEmail("user@localhost");
		request.setPid("PID");
		request.setState(MaterialRequest.VALIDATED);
		materialRequestRepository.save(request);

		MaterialSubRequest subrequest = new MaterialSubRequest();
		subrequest.setInstCode(TEST_INSTCODE);
		subrequest.setInstEmail("institute@localhost");
		subrequest.setSourceRequest(request);
		subrequest.setState(MaterialSubRequest.NOTCONFIRMED);
		materialSubRequestRepository.save(subrequest);
		requestUuid = UUID.fromString(subrequest.getUuid());
	}

	@After
	public void tearDown() {
		instituteRepository.deleteAll();
		materialSubRequestRepository.deleteAll();
		materialRequestRepository.deleteAll();
	}

	@Test
	public void listRequestsTest() throws Exception {
		mockMvc.perform(get("/api/v0/requests/{instCode}", TEST_INSTCODE).param("page", "0")).andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON)).andDo(r -> {
					System.err.println(r.getResponse().getContentAsString());
				}).andExpect(jsonPath("$.size", is(10))).andExpect(jsonPath("$.first", is(true)))
				.andDo(document("requests-inst-list", pathParameters(parameterWithName("instCode").description("Institute WIEWS code (e.g. NGA039)")),
						requestParameters(parameterWithName("page").description("Page to request from the server")),
						responseFields(fieldWithPath("content").description("Array containing request headers"),
								fieldWithPath("numberOfElements").description("Number of records in the content array"),
								fieldWithPath("totalElements").description("Total number of requests in Genesys belonging to this institute"),
								fieldWithPath("size").description("Page size"), fieldWithPath("number").description("Current page number"),
								fieldWithPath("totalPages").description("Page count"), fieldWithPath("sort").description("Arrray sorting details"),
								fieldWithPath("first").ignored(), fieldWithPath("last").ignored())));
	}

	// /requests/{instCode}/r/{uuid:.{36}}
	@Test
	public void getRequestInfoTest() throws Exception {
		mockMvc.perform(get("/api/v0/requests//{instCode}/r/{uuid}", TEST_INSTCODE, requestUuid.toString()).contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON)).andDo(r -> {
					System.err.println(r.getResponse().getContentAsString());
				})
				// .andExpect(jsonPath("$", hasSize(1)))
				.andExpect(jsonPath("$.uuid", is(requestUuid.toString()))).andExpect(jsonPath("$.instCode", is(TEST_INSTCODE)))
				.andDo(document("requests-inst-details",
						pathParameters(parameterWithName("instCode").description("Institute WIEWS code (e.g. NGA039)"),
								parameterWithName("uuid").description("UUID of the request")),
						responseFields(fieldWithPath("uuid").description("The request UUID"), fieldWithPath("version").description("Record version indicator"),
								fieldWithPath("instCode").description("WIEWS code of the institute handling the request"),
								fieldWithPath("instEmail").description("Email address to which Genesys sends notifications"),
								fieldWithPath("state").description("The request STATE indicator"), fieldWithPath("createdDate").ignored(),
								fieldWithPath("lastModifiedDate").ignored(), fieldWithPath("lastReminderDate").description("Date of last email notification sent to institute"))));
	}
}
