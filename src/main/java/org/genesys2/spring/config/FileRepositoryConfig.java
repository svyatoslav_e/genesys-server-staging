/**
 * Copyright 2016 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.spring.config;

import java.io.File;

import org.apache.commons.lang.StringUtils;
import org.genesys2.server.filerepository.service.BytesStorageService;
import org.genesys2.server.filerepository.service.aspect.ImageGalleryAspects;
import org.genesys2.server.filerepository.service.impl.FilesystemStorageServiceImpl;
import org.genesys2.server.filerepository.service.impl.S3StorageServiceImpl;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;

/**
 * Instatiates and configures the storage mechanism for the FileRepository.
 */
@Configuration
public class FileRepositoryConfig {

	@Value("${file.repository.dir}")
	private String fileRepositoryDir;

	@Value("${s3.accessKey}")
	private String s3AccessKey;

	@Bean(name = "fileSystemStorage")
	@Primary
	public BytesStorageService bytesStorageService() {
		if (StringUtils.isNotBlank(s3AccessKey)) {
			// We have some S3 configuration, let's do that!
			return new S3StorageServiceImpl();
		} else {
			// Stick to FS storage
			return fileSystemStorage();
		}
	}

	private BytesStorageService fileSystemStorage() {
		FilesystemStorageServiceImpl storageService = new FilesystemStorageServiceImpl();

		if (fileRepositoryDir != null) {
			File repoDir = new File(fileRepositoryDir);
			storageService.setRepositoryBaseDirectory(repoDir);
		} else {
			// Using tmpdir
			storageService.setRepositoryBaseDirectory(new File(System.getProperty("java.io.tmpdir")));
		}
		return storageService;
	}

	@Bean
	public CommonsMultipartResolver multipartResolver() {
		CommonsMultipartResolver resolver = new CommonsMultipartResolver();
		resolver.setDefaultEncoding("utf-8");
		return resolver;
	}

	/**
	 * Register ImageGallery aspects for automatic registration of new images in
	 * galleries
	 *
	 * @return the image gallery aspects
	 */
	@Bean
	public ImageGalleryAspects imageGalleryAspects() {
		return new ImageGalleryAspects();
	}

}
