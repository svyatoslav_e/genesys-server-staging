package org.genesys2.server.persistence.domain.dataset;

import org.genesys2.server.model.dataset.DS;
import org.genesys2.server.model.dataset.DSRowQualifier;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

public interface DSRowQualifierRepository extends JpaRepository<DSRowQualifier<?>, Long> {

	@Modifying
	@Query("delete from DSRowQualifier dsrq where dsrq.datasetQualifier in (select dsq from DSQualifier dsq where dsq.dataset = ?1)")
	int deleteFor(DS ds);

}
