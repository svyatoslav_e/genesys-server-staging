/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.model.genesys;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Version;

import org.genesys2.server.model.BusinessModel;

/**
 * Accession "alias"
 */
@Entity
@Table(name = "accessionalias")
public class AccessionAlias extends BusinessModel implements AccessionRelated {

	
	private static final long serialVersionUID = 4990299133164025782L;

	public static enum AliasType {
		ACCENAME(0), DONORNUMB(1), BREDNUMB(2), DATABASEID(3), LOCALNAME(4), OTHERNUMB(5), COLLNUMB(6);

		private int id;

		private AliasType(int id) {
			this.id = id;
		}

		public static AliasType getType(Integer id) {
			if (id == null) {
				return null;
			}

			for (final AliasType aliasType : AliasType.values()) {
				if (id.equals(aliasType.getId())) {
					return aliasType;
				}
			}

			throw new IllegalArgumentException("No matching type for id " + id);
		}

		public int getId() {
			return id;
		}
	}

	@Version
	private long version = 0;

	@ManyToOne(optional = false, fetch = FetchType.LAZY, cascade = {})
	@JoinColumn(name = "accessionId", nullable = false, updatable = false)
	private AccessionId accession;

	@Column(name = "name", length = 150)
	private String name;

	@Column(length = 10)
	private String instCode;

	@Column
	private int aliasType = AliasType.ACCENAME.id;

	@Column(length = 2)
	private String lang;

	@Column(length = 64)
	private String usedBy;

	public AccessionAlias() {
	}

	public long getVersion() {
		return version;
	}

	public void setVersion(long version) {
		this.version = version;
	}

	@Override
	public AccessionId getAccession() {
		return accession;
	}

	public void setAccession(AccessionId accession) {
		this.accession = accession;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getInstCode() {
		return instCode;
	}

	public void setInstCode(String instCode) {
		this.instCode = instCode;
	}

	public AliasType getAliasType() {
		return AliasType.getType(this.aliasType);
	}

	public void setAliasType(AliasType aliasType) {
		this.aliasType = aliasType.getId();
	}

	public String getLang() {
		return lang;
	}

	public void setLang(String lang) {
		this.lang = lang;
	}

	public String getUsedBy() {
		return usedBy;
	}

	public void setUsedBy(String usedBy) {
		this.usedBy = usedBy;
	}

	@Override
	public String toString() {
		return "name=" + name + " instCode=" + instCode + " type=" + aliasType;
	}
}
