/*
 * Copyright 2016 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.model.genesys;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.genesys2.server.model.impl.FaoInstitute;
import org.springframework.data.annotation.CreatedDate;

/**
 * This is a copy of the SGVS seed samples record. An accession may be deposited to SGSV several times.
 */
@Entity
@Table(name = "sgsvdeposit", indexes = { @Index(name = "IX_sgsvAID3", unique = false, columnList = "instId,genus,accenumb") })
public class SvalbardDeposit implements AccessionRelated, java.io.Serializable {

	private static final long serialVersionUID = -5015395025122055617L;

	@Id
	@Column(name = "id")
	// SGSV record ID. This is not auto-generated as we mirror SGSV data
	private Long id;

	@ManyToOne(optional = true, cascade = {}, fetch = FetchType.LAZY)
	@JoinColumn(name = "providerId")
	private FaoInstitute provider;

	@ManyToOne(optional = false, cascade = {}, fetch = FetchType.LAZY)
	@JoinColumn(name = "instId")
	private FaoInstitute institute;

	@Column(name = "accenumb", nullable = false, length = 50)
	private String acceNumb;
	@Column(length = 50)
	private String genus;

	// Genesys accession reference
	@ManyToOne(optional = true, cascade = {}, fetch = FetchType.LAZY)
	@JoinColumn(name = "acceId")
	private AccessionId accession;

	private Float quantity;

	@Temporal(TemporalType.DATE)
	private Date depositDate;
	@Column(length = 20)
	private String boxNo;
	@Column(name = "regen", length = 50)
	private String regenerationInformation;

	@CreatedDate
	@Column(name = "createdDate")
	private Date createdDate;

	public String getAcceNumb() {
		return acceNumb;
	}

	@Override
	public AccessionId getAccession() {
		return accession;
	}

	public String getBoxNo() {
		return boxNo;
	}

	public String getBoxNumber() {
		return boxNo;
	}

	public Long getId() {
		return id;
	}

	public Float getQuantity() {
		return quantity;
	}

	public void setAcceNumb(String acceNumb) {
		this.acceNumb = acceNumb;
	}

	public void setAccession(AccessionId accession) {
		this.accession = accession;
	}

	public void setBoxNo(String boxNo) {
		this.boxNo = boxNo;
	}

	public void setBoxNumber(String boxNo) {
		this.boxNo = boxNo;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setQuantity(Float quantity) {
		this.quantity = quantity;
	}

	public String getGenus() {
		return genus;
	}

	public void setGenus(String genus) {
		this.genus = genus;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	protected void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public FaoInstitute getProvider() {
		return provider;
	}

	public void setProvider(FaoInstitute provider) {
		this.provider = provider;
	}

	public FaoInstitute getInstitute() {
		return institute;
	}

	public void setInstitute(FaoInstitute institute) {
		this.institute = institute;
	}

	public Date getDepositDate() {
		return depositDate;
	}

	public void setDepositDate(Date depositDate) {
		this.depositDate = depositDate;
	}
	
	public String getRegenerationInformation() {
		return regenerationInformation;
	}
	
	public void setRegenerationInformation(String regenerationInformation) {
		this.regenerationInformation = regenerationInformation;
	}
}
