package org.genesys2.server.model.filters;

public class ValueName<T> {
	private final T value;
	private final String name;
	private Long count;

	public ValueName(T value, String name) {
		this.value = value;
		this.name = name;
	}

	public ValueName(T value, String name, Long count) {
		this.value = value;
		this.name = name;
		this.count = count;
	}

	public T getValue() {
		return value;
	}

	public String getName() {
		return name;
	}

	public Long getCount() {
		return count;
	}
}