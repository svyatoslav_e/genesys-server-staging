package org.genesys2.server.model.elastic;

import org.apache.commons.lang.StringUtils;
import org.genesys2.server.model.genesys.AccessionAlias;
import org.genesys2.server.model.genesys.AccessionAlias.AliasType;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldIndex;
import org.springframework.data.elasticsearch.annotations.FieldType;

public class Alias {

	private String name;
	@Field(index = FieldIndex.not_analyzed, type = FieldType.String)
	private String lang;
	@Field(index = FieldIndex.not_analyzed, type = FieldType.String)
	private String usedBy;
	@Field(index = FieldIndex.not_analyzed, type = FieldType.String)
	private AliasType type;

	public static Alias from(AccessionAlias aa) {
		Alias a = new Alias();
		a.name = aa.getName();
		a.lang = StringUtils.defaultIfBlank(aa.getLang(), null);
		a.usedBy = StringUtils.defaultIfBlank(aa.getUsedBy(), null);
		a.type = aa.getAliasType();
		return a;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLang() {
		return lang;
	}

	public void setLang(String lang) {
		this.lang = lang;
	}

	public String getUsedBy() {
		return usedBy;
	}

	public void setUsedBy(String usedBy) {
		this.usedBy = usedBy;
	}

	public AliasType getType() {
		return type;
	}

	public void setType(AliasType type) {
		this.type = type;
	}
}