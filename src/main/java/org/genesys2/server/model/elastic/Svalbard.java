package org.genesys2.server.model.elastic;

import java.util.Date;

import org.genesys2.server.model.genesys.SvalbardDeposit;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldIndex;
import org.springframework.data.elasticsearch.annotations.FieldType;

/**
 * One SGSV deposit
 */
public class Svalbard {

	private Float qty;
	@Field(index = FieldIndex.not_analyzed, type = FieldType.String)
	private String boxNo;
	@Field(index = FieldIndex.not_analyzed, type = FieldType.Date)
	private Date depositDate;

	public static Svalbard from(SvalbardDeposit svalbardData) {
		if (svalbardData == null)
			return null;
		Svalbard s = new Svalbard();
		s.boxNo = svalbardData.getBoxNumber();
		s.qty = svalbardData.getQuantity();
		s.depositDate = svalbardData.getDepositDate();
		return s;
	}

	public Float getQty() {
		return qty;
	}

	public void setQty(Float qty) {
		this.qty = qty;
	}

	public String getBoxNo() {
		return boxNo;
	}

	public void setBoxNo(String boxNo) {
		this.boxNo = boxNo;
	}

	public Date getDepositDate() {
		return depositDate;
	}

	public void setDepositDate(Date depositDate) {
		this.depositDate = depositDate;
	}

}