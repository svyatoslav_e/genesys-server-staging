/**
 * Copyright 2016 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.service.impl;

import java.io.IOException;
import java.util.UUID;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.genesys2.server.filerepository.InvalidRepositoryFileDataException;
import org.genesys2.server.filerepository.InvalidRepositoryPathException;
import org.genesys2.server.filerepository.NoSuchRepositoryFileException;
import org.genesys2.server.filerepository.model.ImageGallery;
import org.genesys2.server.filerepository.model.RepositoryFile;
import org.genesys2.server.filerepository.model.RepositoryImage;
import org.genesys2.server.filerepository.model.RepositoryImageData;
import org.genesys2.server.filerepository.service.ImageGalleryService;
import org.genesys2.server.filerepository.service.RepositoryService;
import org.genesys2.server.model.genesys.Accession;
import org.genesys2.server.model.impl.FaoInstitute;
import org.genesys2.server.service.InstituteFilesService;
import org.genesys2.spring.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;

/**
 * Security checks & co.
 */
@Service
public class InstituteFilesServiceImpl implements InstituteFilesService {

	public static final Log LOG = LogFactory.getLog(InstituteFilesServiceImpl.class);

	private static final String REPOSITORY_GALLERIES_PREFIX = "/accessions/";

	private static final String ACCESSION_GALLERY_TITLE_FORMAT = "%1s image gallery";

	private static final String ACCESSION_GALLERY_DESCRIPTION = null;

	@Autowired
	private RepositoryService repositoryService;

	@Autowired
	private ImageGalleryService imageGalleryService;

	private static final String getGalleriesPrefix(final FaoInstitute institute) {
		return REPOSITORY_GALLERIES_PREFIX + institute.getCode() + "/acn/";
	}

	private static final String getGalleryPath(final FaoInstitute institute, final Accession accession) {
		return getGalleriesPrefix(institute) + accession.getAccessionName() + "/";
	}

	@Override
	public Page<ImageGallery> listImageGalleries(FaoInstitute institute, Pageable pageable) {
		return imageGalleryService.listImageGalleries(getGalleriesPrefix(institute), pageable);
	}

	@Override
	public ImageGallery loadImageGallery(final FaoInstitute institute, final Accession accession) {
		final String path = getGalleryPath(institute, accession);
		return this.imageGalleryService.loadImageGallery(path);
	}

	@Override
	@PreAuthorize("hasRole('ADMINISTRATOR') or hasPermission(#institute, 'WRITE') or hasPermission(#institute, 'CREATE')")
	public ImageGallery createImageGallery(FaoInstitute institute, Accession accession) {
		final String path = getGalleryPath(institute, accession);

		return this.imageGalleryService.createImageGallery(path, String.format(ACCESSION_GALLERY_TITLE_FORMAT, accession.getAccessionName()), ACCESSION_GALLERY_DESCRIPTION);
	}

	@Override
	@PreAuthorize("hasRole('ADMINISTRATOR') or hasPermission(#institute, 'WRITE') or hasPermission(#institute, 'CREATE')")
	public RepositoryImage getImage(final FaoInstitute institute, final Accession accession, final UUID uuid) throws NoSuchRepositoryFileException {
		final RepositoryFile repositoryFile = this.repositoryService.getFile(uuid);

		if (!repositoryFile.getPath().equals(getGalleryPath(institute, accession))) {
			LOG.warn(repositoryFile.getPath() + "!=" + getGalleryPath(institute, accession));
			throw new ResourceNotFoundException("No such thing");
		}

		// TODO Fix the path before returning
		return (RepositoryImage) repositoryFile;
	}

	@Override
	@PreAuthorize("hasRole('ADMINISTRATOR') or hasPermission(#institute, 'WRITE') or hasPermission(#institute, 'CREATE')")
	public RepositoryImage updateImageMetadata(final FaoInstitute institute, Accession accession, final UUID uuid, final RepositoryImageData imageData) throws NoSuchRepositoryFileException {
		final RepositoryFile repositoryFile = this.repositoryService.getFile(uuid);
		if (!repositoryFile.getPath().equals(getGalleryPath(institute, accession))) {
			LOG.warn(repositoryFile.getPath() + "!=" + getGalleryPath(institute, accession));
			throw new ResourceNotFoundException("No such thing");
		}
		return this.repositoryService.updateImageMetadata(repositoryFile.getUuid(), imageData);
	}

	@Override
	@PreAuthorize("hasRole('ADMINISTRATOR') or hasPermission(#institute, 'WRITE') or hasPermission(#institute, 'CREATE')")
	public byte[] getFileBytes(final FaoInstitute institute, final Accession accession, final RepositoryImage repositoryImage) throws NoSuchRepositoryFileException {
		return this.repositoryService.getFileBytes(getGalleryPath(institute, accession), repositoryImage.getFilename());
	}

	@Override
	@PreAuthorize("hasRole('ADMINISTRATOR') or hasPermission(#institute, 'WRITE') or hasPermission(#institute, 'CREATE')")
	public RepositoryImage addImage(FaoInstitute institute, Accession accession, String originalFilename, String contentType, byte[] bytes)
			throws InvalidRepositoryPathException, InvalidRepositoryFileDataException, IOException {
		return repositoryService.addImage(getGalleryPath(institute, accession), originalFilename, contentType, bytes, null);
	}

	@Override
	@PreAuthorize("hasRole('ADMINISTRATOR') or hasPermission(#institute, 'WRITE') or hasPermission(#institute, 'CREATE')")
	public RepositoryImage updateImage(FaoInstitute institute, Accession accession, RepositoryImage repositoryImage, String contentType, byte[] data)
			throws NoSuchRepositoryFileException, IOException {
		return repositoryService.updateImageBytes(repositoryImage, contentType, data);
	}

	@Override
	@PreAuthorize("hasRole('ADMINISTRATOR') or hasPermission(#institute, 'ADMINISTRATE') or hasPermission(#institute, 'DELETE')")
	public RepositoryImage deleteImage(FaoInstitute institute, Accession accession, RepositoryImage repositoryImage) throws NoSuchRepositoryFileException, IOException {
		return repositoryService.removeImage(repositoryImage);
	}

}
