/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.service.impl;

public class NonUniqueAccessionException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6540050553016489789L;
	private String instCode;
	private String acceNumb;
	private String genus;

	public NonUniqueAccessionException(String instCode, String acceNumb) {
		this.instCode = instCode;
		this.acceNumb = acceNumb;
	}

	public NonUniqueAccessionException(String instCode, String acceNumb, String genus) {
		this.instCode = instCode;
		this.acceNumb = acceNumb;
		this.genus = genus;
	}

	public String getInstCode() {
		return instCode;
	}

	public String getAcceNumb() {
		return acceNumb;
	}

	@Override
	public String getMessage() {
		return "Non-unique accession instCode=" + instCode + " acceNumb=" + acceNumb + (genus == null ? "" : " genus=" + genus);
	}

}
