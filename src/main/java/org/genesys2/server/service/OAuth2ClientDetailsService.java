/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.service;

import java.util.Collection;
import java.util.List;

import org.genesys2.server.model.oauth.OAuthAccessToken;
import org.genesys2.server.model.oauth.OAuthClientDetails;
import org.genesys2.server.model.oauth.OAuthClientType;
import org.genesys2.server.model.oauth.OAuthRefreshToken;
import org.springframework.security.oauth2.provider.ClientDetailsService;

public interface OAuth2ClientDetailsService extends ClientDetailsService {

	Collection<OAuthAccessToken> findTokensByClientId(String clientId);
	Collection<OAuthRefreshToken> findRefreshTokensClientId(String clientId);

	Collection<OAuthAccessToken> findTokensByUserUuid(String uuid);

	OAuthClientDetails addClientDetails(String title, String description, String redirectUri, Integer accessTokenValiditySeconds, Integer refreshTokenValiditySeconds, OAuthClientType clientType);
	OAuthClientDetails update(OAuthClientDetails clientDetails, String title, String description, String clientSecret, String redirectUris, Integer accessTokenValiditySeconds, Integer refreshTokenValiditySeconds);

	List<OAuthClientDetails> listClientDetails();

	/**
	 * Returns client details if user has permission to manage
	 * 
	 * @param clientId
	 * @return
	 */
	OAuthClientDetails getClientDetails(long clientId);

	void removeClient(OAuthClientDetails clientDetails);


}
