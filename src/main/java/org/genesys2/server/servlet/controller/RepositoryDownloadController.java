/**
 * Copyright 2016 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.servlet.controller;

import java.io.IOException;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.velocity.exception.ResourceNotFoundException;
import org.genesys2.server.filerepository.NoSuchRepositoryFileException;
import org.genesys2.server.filerepository.model.RepositoryFile;
import org.genesys2.server.filerepository.service.BytesStorageService;
import org.genesys2.server.filerepository.service.RepositoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.HandlerMapping;

@Controller
public class RepositoryDownloadController extends BaseController {

	public static final Log LOG = LogFactory.getLog(RepositoryDownloadController.class);

	@Autowired
	private RepositoryService repositoryService;

	@Autowired
	private BytesStorageService byteStorageService;

	private void downloadFile(final String path, final String uuid, final String ext, final HttpServletResponse response) throws IOException {
		byte[] data;

		if (path.endsWith("_thumb/") && ext.equals(".png")) {
			final String filename = uuid + ext;
			if (LOG.isDebugEnabled()) {
				LOG.debug("_thumb path=" + path + " filename=" + filename);
			}

			data = this.byteStorageService.get(path, filename);
			response.setContentType("image/png");

		} else {
			// Regular repo file
			try {
				final RepositoryFile repositoryFile = this.repositoryService.getFile(UUID.fromString(uuid));

				sanityCheck(path, ext, repositoryFile);

				data = this.repositoryService.getFileBytes(repositoryFile.getPath(), repositoryFile.getFilename());

				response.setHeader(HttpHeaders.CACHE_CONTROL, "max-age=3600, s-maxage=3600, public, no-transform");
				response.setHeader(HttpHeaders.PRAGMA, "");
				response.setDateHeader(HttpHeaders.LAST_MODIFIED, repositoryFile.getLastModifiedDate().getTime());
				response.setContentType(repositoryFile.getContentType());
				response.addHeader("Content-Disposition", String.format("attachment; filename=\"%s\"", repositoryFile.getOriginalFilename()));

			} catch (final NumberFormatException e) {
				LOG.warn("404 - UUID in wrong format.");
				throw new ResourceNotFoundException("No such thing");
			} catch (final NoSuchRepositoryFileException e) {
				LOG.warn("404 - No such repository file ", e);
				throw new ResourceNotFoundException("No such thing");
			}
		}

		if (data != null) {
			response.setContentLength(data.length);
			response.getOutputStream().write(data);
		}
		response.flushBuffer();
	}

	private void sanityCheck(final String path, final String ext, final RepositoryFile repositoryFile) {
		if (repositoryFile == null) {
			throw new ResourceNotFoundException("No such thing");
		}

		if (!repositoryFile.getPath().equals(path) || !repositoryFile.getExtension().equals(ext)) {
			LOG.warn(repositoryFile.getPath() + "!=" + path);
			LOG.warn(repositoryFile.getExtension() + "!=" + ext);
			throw new ResourceNotFoundException("No such thing");
		}
	}

	/**
	 * Serve the bytes of the repository object
	 */
	@RequestMapping(value = "/repository/d/**", method = RequestMethod.GET)
	public void downloadFile(final HttpServletRequest request, final HttpServletResponse response) throws IOException {
		final String fullpath = (String) request.getAttribute(HandlerMapping.PATH_WITHIN_HANDLER_MAPPING_ATTRIBUTE);
		if (LOG.isTraceEnabled()) {
			LOG.trace("Fullname: " + fullpath);
		}

		final String ext = fullpath.substring(fullpath.lastIndexOf("."));
		final String uuid = fullpath.substring(fullpath.lastIndexOf("/") + 1, fullpath.lastIndexOf("."));
		final String path = fullpath.substring("/repository/d".length(), fullpath.lastIndexOf("/") + 1);

		if (LOG.isDebugEnabled()) {
			LOG.debug(path + " " + uuid + ext);
		}

		downloadFile(path, uuid, ext, response);
	}

	/**
	 * Return repository object metadata
	 */
	@RequestMapping(value = "/repository/d/**", method = RequestMethod.GET, params = { "metadata" }, produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody RepositoryFile getMetadata(final HttpServletRequest request) throws IOException, NoSuchRepositoryFileException {

		final String fullpath = (String) request.getAttribute(HandlerMapping.PATH_WITHIN_HANDLER_MAPPING_ATTRIBUTE);
		if (LOG.isTraceEnabled()) {
			LOG.trace("Fullname: " + fullpath);
		}

		String path;
		String uuid;
		String ext;
		try {
			ext = fullpath.substring(fullpath.lastIndexOf("."));
			uuid = fullpath.substring(fullpath.lastIndexOf("/") + 1, fullpath.lastIndexOf("."));
			path = fullpath.substring("/repository/d".length(), fullpath.lastIndexOf("/") + 1);
			if (LOG.isDebugEnabled()) {
				LOG.debug(path + " " + uuid + ext);
			}
		} catch (ArrayIndexOutOfBoundsException e) {
			// fullpath.lastIndexOf may return -1, causing AIOBE
			throw new ResourceNotFoundException("No such resource " + fullpath);
		}


		final RepositoryFile repositoryFile = this.repositoryService.getFile(UUID.fromString(uuid));

		sanityCheck(path, ext, repositoryFile);

		return repositoryFile;
	}
}
