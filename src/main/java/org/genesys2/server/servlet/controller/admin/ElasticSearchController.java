/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.servlet.controller.admin;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.elasticsearch.client.Client;
import org.elasticsearch.cluster.metadata.AliasMetaData;
import org.elasticsearch.cluster.metadata.IndexMetaData;
import org.elasticsearch.common.collect.ImmutableOpenMap;
import org.elasticsearch.common.hppc.cursors.ObjectObjectCursor;
import org.genesys2.server.model.impl.ActivityPost;
import org.genesys2.server.model.impl.Article;
import org.genesys2.server.model.impl.Country;
import org.genesys2.server.model.impl.FaoInstitute;
import org.genesys2.server.service.ElasticSearchManagementService;
import org.genesys2.server.service.IndexAliasConstants;
import org.genesys2.server.service.impl.FilterHandler;
import org.genesys2.server.service.worker.ElasticUpdater;
import org.genesys2.server.service.worker.ElasticUpdater.ElasticNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.hazelcast.core.IQueue;

@Controller
@RequestMapping("/admin/elastic")
@PreAuthorize("hasRole('ADMINISTRATOR')")
public class ElasticSearchController {
	public static final Log LOG = LogFactory.getLog(ElasticSearchController.class);

	@Autowired
	private ElasticUpdater elasticUpdater;

	@Autowired
	private ElasticSearchManagementService elasticSearchManagementService;

	@Resource
	private IQueue<ElasticNode> elasticRemoveQueue;

	@Resource
	private IQueue<ElasticNode> elasticUpdateQueue;
	
	@Autowired
	private Client client;

	private Map<String, String> reindexTypes = createReindexTypesMap();

	ObjectMapper mapper = new ObjectMapper();

	/**
	 * Renders view where indexes and their aliases are displayed.
	 *
	 * @param model
	 * @return
	 */
	@RequestMapping("/")
	public String viewIndexesAndAliases(Model model) {
		ImmutableOpenMap<String, IndexMetaData> indicesImmutableMap = client.admin().cluster().prepareState().execute().actionGet().getState().getMetaData()
				.getIndices();

		Map<String, List<AliasMetaData>> indexMap = new HashMap<>();
		for (ObjectObjectCursor<String, IndexMetaData> cursor : indicesImmutableMap) {
			List<AliasMetaData> innerAliasList = new ArrayList<>();
			for (ObjectObjectCursor<String, AliasMetaData> innerCursor : cursor.value.getAliases()) {
				innerAliasList.add(innerCursor.value);
			}
			indexMap.put(cursor.key, innerAliasList);
		}

		model.addAttribute("indexes", indexMap);
		model.addAttribute("reindexTypes", reindexTypes);

		model.addAttribute("removeQueueSize", elasticRemoveQueue.size());
		model.addAttribute("updateQueueSize", elasticUpdateQueue.size());
		
		return "/admin/elastic/index";
	}

	/**
	 * Completely recreate Elasticsearch indexes: create, index, re-alias.
	 *
	 * @return
	 */
	@RequestMapping(method = RequestMethod.POST, value = "/action", params = { "regenerate=accn" })
	public String regenerateElastic() {
		elasticSearchManagementService.regenerateIndexes(IndexAliasConstants.INDEX_PASSPORT);
		return "redirect:/admin/elastic/";
	}

	/**
	 * Completely recreate Elasticsearch indexes: create, index, re-alias.
	 *
	 * @return
	 */
	@RequestMapping(method = RequestMethod.POST, value = "/action", params = { "regenerate=content" })
	public String regenerateElasticContent() {
		elasticSearchManagementService.regenerateIndexes(IndexAliasConstants.INDEX_FULLTEXT);
		return "redirect:/admin/elastic/";
	}

	/**
	 * This method refreshes data in the currently active index. It is very
	 * handy when having to refresh part of ES after direct database update.
	 *
	 * @param jsonFilter
	 * @throws IOException
	 */
	@RequestMapping(method = RequestMethod.POST, value = "/action", params = { "reindex=accn", "filter" })
	public String reindexElasticFiltered(@RequestParam(value = "filter", required = true) String jsonFilter) throws IOException {

		FilterHandler.AppliedFilters filters = mapper.readValue(jsonFilter, FilterHandler.AppliedFilters.class);

		elasticSearchManagementService.reindex(filters);

		return "redirect:/admin/elastic/";
	}

	/**
	 * This method refreshes data in the currently active index. It is very
	 * handy when having to refresh part of ES after direct database update.
	 *
	 * @param type
	 * @throws IOException
	 */
	@RequestMapping(method = RequestMethod.POST, value = "/action", params = { "reindex=content", "type" })
	public String reindexElasticContent(@RequestParam(value = "type", required = true) String type) throws IOException {

		if (type.equals("All")) {
			elasticSearchManagementService.regenerateIndexes(IndexAliasConstants.INDEX_FULLTEXT);
		} else {
			elasticSearchManagementService.reindex(type);
		}

		return "redirect:/admin/elastic/";
	}

	/**
	 * Clear ES queue
	 *
	 * @return
	 */
	@RequestMapping(method = RequestMethod.POST, value = "/action", params = { "clear-queues" })
	public String clearElasticQueues() {
		elasticUpdater.clearQueues();
		return "redirect:/admin/elastic/";
	}

	@RequestMapping(method = RequestMethod.POST, value = "/action", params = { "action=realias" })
	public String moveAlias(@RequestParam(name = "aliasName") String aliasName, @RequestParam(name = "indexName") String indexName) {
		elasticSearchManagementService.realias(aliasName, indexName);
		return "redirect:/admin/elastic/";
	}

	@RequestMapping(method = RequestMethod.POST, value = "/action", params = { "action=delete-alias" })
	public String deleteAlias(@RequestParam(name = "aliasName") String aliasName) {
		elasticSearchManagementService.deleteAlias(aliasName);
		return "redirect:/admin/elastic/";
	}

	@RequestMapping(method = RequestMethod.POST, value = "/action", params = { "action=delete-index", "indexName" })
	public String deleteIndex(@RequestParam(name = "indexName") String indexName) {
		elasticSearchManagementService.deleteIndex(indexName);
		return "redirect:/admin/elastic/";
	}

	private Map<String, String> createReindexTypesMap() {
		Map<String, String> reindexTypesMap = new HashMap<>();

		reindexTypesMap.put(Article.class.getSimpleName(), Article.class.getName());
		reindexTypesMap.put(ActivityPost.class.getSimpleName(), ActivityPost.class.getName());
		reindexTypesMap.put(Country.class.getSimpleName(), Country.class.getName());
		reindexTypesMap.put(FaoInstitute.class.getSimpleName(), FaoInstitute.class.getName());

		return reindexTypesMap;
	}
}
