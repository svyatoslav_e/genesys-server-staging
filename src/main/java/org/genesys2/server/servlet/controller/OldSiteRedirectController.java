/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.servlet.controller;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

/**
 * Controller which handles requests of old genesys
 */
@Controller
public class OldSiteRedirectController extends BaseController {

	@RequestMapping(value = "/Accession.php", params = "lookfor", method = RequestMethod.GET)
	public ModelAndView accessionPhp(@RequestParam("lookfor") long accessionId) {
		final RedirectView rv = new RedirectView("/acn/id/" + accessionId);
		rv.setStatusCode(HttpStatus.MOVED_PERMANENTLY);
		final ModelAndView mv = new ModelAndView(rv);
		_logger.info("Permanent redirect to: " + rv.getUrl());
		return mv;
	}

}
