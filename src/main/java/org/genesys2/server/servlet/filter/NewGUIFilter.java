/**
 * Copyright 2016 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.servlet.filter;

import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.genesys2.spring.config.NewGUIViewResolver;
import org.springframework.web.util.UriUtils;

/**
 * The {@code NewGUIFilter} allows for having URL-based versioned user
 * interfaces. Any URL prefixed with {@code /version/} is stripped of the
 * version information and the version data stored. The decorators are loaded
 * before this filter and will appropriately render appropriately.
 *
 * @author Matija Obreza
 */
public class NewGUIFilter implements Filter {
	private static final Logger LOG = Logger.getLogger(NewGUIFilter.class);
	public static final String REQUEST_GUIVERSION_ATTR = LocaleURLFilter.class.getName() + ".GUIVERSION";

	private static final Pattern newGuiPattern = Pattern.compile("/(\\d+)(/.+)");

	private String[] excludedPaths;

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		String excludePaths = filterConfig.getInitParameter("exclude-paths");
		if (StringUtils.isNotBlank(excludePaths)) {
			String[] ex = excludePaths.split("\\s+");
			for (String e : ex) {
				LOG.info("Excluding path: " + e);
			}
			this.excludedPaths = ex;
		}
	}

	@Override
	public void destroy() {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Destroying NewGUIFilter");
		}
	}

	@Override
	public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
		final HttpServletRequest httpRequest = (HttpServletRequest) servletRequest;
		HttpServletResponse httpResponse = (HttpServletResponse) servletResponse;
		final String originalUrl = httpRequest.getRequestURI().substring(httpRequest.getContextPath().length());
		final String origianlUrlDecoded = UriUtils.decode(originalUrl, "UTF-8");

		if (LOG.isTraceEnabled()) {
			LOG.trace("Incoming URL: " + originalUrl);
		}

		final Matcher matcher = newGuiPattern.matcher(originalUrl);
		if (matcher.matches()) {
			final String guiPrefix = "/" + matcher.group(1);
			final String remainingUrl = matcher.group(2);

			if (LOG.isDebugEnabled()) {
				LOG.trace("URL matches! prefix=" + guiPrefix + " remaining=" + remainingUrl);
				LOG.debug("Proxying request to remaining URL " + remainingUrl);
			}

			HttpServletRequestWrapper guiRequest = new HttpServletRequestWrapper(httpRequest) {

				@Override
				public String getServletPath() {
					String servletPath = super.getServletPath();
					// servletPath is URL decoded, must use origianlUrlDecoded.
					if (origianlUrlDecoded.equals(servletPath)) {
						if (LOG.isDebugEnabled())
							LOG.debug("servletPath=" + servletPath + " remaining=" + remainingUrl);
						return remainingUrl;
					}
					return servletPath;
				}

				@Override
				public String getRequestURI() {
					String requestURI = super.getRequestURI();
					// requestURI is URL encoded, must use originalUrl.
					if (originalUrl.equals(requestURI)) {
						if (LOG.isDebugEnabled())
							LOG.debug("requestURI=" + requestURI + " remaining=" + remainingUrl);
						return remainingUrl;
					}
					return requestURI;
				}
			};
			HttpServletResponseWrapper guiResponse = new HttpServletResponseWrapper(httpResponse) {
				private boolean isExcluded(String url) {
					for (String excludedPath : excludedPaths) {
						if (url.startsWith(excludedPath)) {
							if (LOG.isTraceEnabled()) {
								LOG.trace("Excluded=" + excludedPath + " matches " + url);
							}
							return true;
						}
					}
					return url.startsWith("?");
				}

				@Override
				public String encodeURL(String url) {
					if (isExcluded(url)) {
						return super.encodeURL(url);
					} else {
						String encodedURL = super.encodeURL(guiPrefix + url);
						if (LOG.isDebugEnabled()) {
							LOG.debug("encodeURL " + url + " to " + encodedURL);
						}
						return encodedURL;
					}
				}

				@Override
				@Deprecated
				public String encodeUrl(String url) {
					if (isExcluded(url)) {
						return super.encodeUrl(url);
					} else {
						String encodedURL = super.encodeUrl(guiPrefix + url);
						if (LOG.isDebugEnabled()) {
							LOG.debug("encodeUrl " + url + " to " + encodedURL);
						}
						return encodedURL;
					}
				}

				@Override
				public String encodeRedirectURL(String url) {
					if (isExcluded(url)) {
						return super.encodeRedirectURL(url);
					} else {
						String encodedURL = super.encodeRedirectURL(guiPrefix + url);
						if (LOG.isDebugEnabled()) {
							LOG.debug("encodeRedirectURL " + url + " to " + encodedURL);
						}
						return encodedURL;
					}
				}
			};

			try {
				try {
					final int guiVersion = Integer.parseInt(matcher.group(1));
					httpRequest.setAttribute(REQUEST_GUIVERSION_ATTR, guiVersion);
					LocalGUIVersion.set(guiVersion);
				} catch (NumberFormatException e) {
					LOG.info("Invalid GUI version in " + matcher.group(1) + ": " + e.getMessage());
				}
				filterChain.doFilter(guiRequest, guiResponse);
			} finally {
				LocalGUIVersion.remove();
			}
		} else {
			if (LOG.isTraceEnabled()) {
				LOG.trace("No match on url " + originalUrl);
			}
			filterChain.doFilter(servletRequest, servletResponse);
		}
	}

	/**
	 * A ThreadLocal utility to manage the GUI version information. Used by
	 * {@link NewGUIViewResolver}.
	 */
	public static class LocalGUIVersion {
		// Thread local variable containing each thread's ID
		private static final ThreadLocal<Integer> threadGuiVersion = new ThreadLocal<Integer>() {
			@Override
			protected Integer initialValue() {
				return 0;
			};
		};

		// Returns the current thread's unique ID, assigning it if necessary
		public static void set(int version) {
			threadGuiVersion.set(version);
		}

		public static void remove() {
			threadGuiVersion.remove();
		}

		// Returns the current thread's unique ID, assigning it if necessary
		public static int get() {
			return threadGuiVersion.get();
		}
	}
}
