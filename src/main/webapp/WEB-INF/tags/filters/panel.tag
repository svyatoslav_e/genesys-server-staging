<%@ tag description="Filter panel" pageEncoding="UTF-8" %>
<%@ tag body-content="scriptless" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="filters" tagdir="/WEB-INF/tags/filters" %>
<%@ attribute name="id" required="true" type="java.lang.String" %>
<%@ attribute name="title" required="true" type="java.lang.String" %>

<div class="panel panel-default" id="panel_${id}">
	<div class="panel-heading">
		<h4 class="panel-title">
			<a data-toggle="collapse" href="#panel_${id}_collapse" class="collapsed">
				<spring:message code="${title}" />
			</a>
		</h4>
	</div>

	<div id="panel_${id}_collapse" class="panel-collapse collapse">
		<div class="panel-body">
			<jsp:doBody />
		</div>		
	</div>
</div>
