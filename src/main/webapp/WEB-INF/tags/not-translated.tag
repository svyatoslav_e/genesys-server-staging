<%@ tag description="Display the non-trasnslated message" pageEncoding="UTF-8" %>
<%@ tag body-content="empty" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ attribute name="display" required="false" description="Should the alert be displayed or not" rtexprvalue="true" type="java.lang.Boolean" %>

<c:if test="${(empty display) or display eq true}">
	<div class="alert alert-warning translationmissing">
		<spring:message code="i18n.content-not-translated" />
	</div>
</c:if>