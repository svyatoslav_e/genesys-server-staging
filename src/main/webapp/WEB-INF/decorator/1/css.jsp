<%@ include file="/WEB-INF/jsp/init.jsp" %>

<c:choose>
  <c:when test="${requestContext.theme.name eq 'one'}">
    <link href="<c:url value="/html/1/styles/all.min.css" />" type="text/css" rel="stylesheet" />
  </c:when>
  <c:when test="${requestContext.theme.name eq 'all'}">
    <link href="<c:url value="/html/1/styles/bootstrap.min.css" />" type="text/css" rel="stylesheet" />
    <link href="<c:url value="/html/1/styles/other.min.css" />" type="text/css" rel="stylesheet" />
    <link href="<c:url value="/html/1/styles/genesys.css" />" type="text/css" rel="stylesheet" />
  </c:when>
  <c:otherwise>
    <link href="<c:url value="/html/1/styles/bootstrap.css" />" type="text/css" rel="stylesheet" />
    <link href="<c:url value="/html/1/styles/other.min.css" />" type="text/css" rel="stylesheet" />
    <link href="<c:url value="/html/1/styles/genesys.css" />" type="text/css" rel="stylesheet" />
  </c:otherwise>
</c:choose>

