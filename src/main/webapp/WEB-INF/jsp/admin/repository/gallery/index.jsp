<!DOCTYPE html>

<%@ include file="/WEB-INF/jsp/init.jsp" %>

<html>
<head>
<title><spring:message code="menu.admin.repository.galleries" /></title>
</head>
<body>
	<!-- Any alerts? -->
	<gui:alert type="warning" display="${not empty errorMessage}">
		<c:out value="${errorMessage}" />
	</gui:alert>
	<gui:alert type="success" display="${not empty successMessage}">
		<c:out value="${successMessage}" />
	</gui:alert>

	<table class="table table-striped">
		<thead>
			<tr>
				<th class="col-xs-5"><spring:message
						code="repository.gallery" /></th>
				<th class="col-xs-3"><spring:message
						code="repository.gallery.path" /></th>
				<th class="col-xs-4"></th>
			</tr>
		</thead>
		<tbody>
			<c:forEach var="gallery" items="${pagedData.content}" varStatus="i">
				<tr>
					<td class="col-md-5"><a href="<c:url value="/admin/r/g${gallery.path}" />"><c:out value="${gallery.title}" /></a></td>
					<td class="col-md-3"><c:out value="${gallery.path}" /></td>
					<td class="col-md-4 text-right">
						<form action="<c:url value="/admin/r/g/delete" />"
							method="post">
							<a
								href="<c:url value="/admin/r/g/edit"><c:param name="galleryPath" value="${gallery.path}" /></c:url>"
								class="btn btn-default"><spring:message code="edit" /></a>
								
							<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
							<input type="hidden" name="galleryPath" value="${gallery.path}" />
							<button type="submit" name="action" value="delete-file" class="btn btn-default confirm-delete"><spring:message code="delete" /></button>
						</form>
					</td>
				</tr>
			</c:forEach>
		</tbody>
	</table>

	<content tag="javascript"> <script type="text/javascript">
		$(document).ready(function() {
			$('.confirm-delete').click(function(ev) {
				if (! window.confirm('<spring:message code="prompt.confirm-delete" />')) {
					ev.stopPropagation();
					return false;
				}
			});
		});
	</script> </content>

</body>
</html>
