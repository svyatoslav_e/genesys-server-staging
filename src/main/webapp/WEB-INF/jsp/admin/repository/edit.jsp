	<!DOCTYPE html>

<%@ include file="/WEB-INF/jsp/init.jsp" %>

<html>
    <head>
        <title><spring:message code="admin.page.title"/></title>
    </head>
    <body>
        <div class="row">
            <div class="col-md-12">
                <a href="<c:url value="/admin/r/files${currentPath}" />" class="btn btn-default"><spring:message code="cancel" /></a>
                <h4>Updating metadata for file '<c:out value="${file.originalFilename}" />'</h4><br/>
                
                <form action="<c:url value="/admin/r/update" />" method="post">
                	<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
                    <input type="hidden" name="uuid" value="${file.uuid}">

                    <div class="form-group">
                        <label for="title"><spring:message code="repository.file.title" /></label>
                        <input type="text" id="title" name="title" value="${file.title}" class="form-control">
                    </div>

                    <div class="form-group">
                        <label for="subject"><spring:message code="repository.file.subject" /></label>
                        <input type="text" id="subject" name="subject" value="${file.subject}" class="form-control">
                    </div>

                    <div class="form-group">
                        <label for="description"><spring:message code="repository.file.description" /></label>
                        <textarea id="description" name="description" class="form-control"><c:out escapeXml="false" value="${file.description}" /></textarea>
                    </div>

                    <div class="form-group">
                        <label for="creator"><spring:message code="repository.file.creator" /></label>
                        <input type="text" id="creator" name="creator" value="${file.creator}" class="form-control">
                    </div>

                    <div class="form-group">
                        <label for="created"><spring:message code="repository.file.created" /></label>
                        <input type="text" id="created" name="created" value="${file.created}" class="form-control">
                    </div>

                    <div class="form-group">
                        <label for="rightsHolder"><spring:message code="repository.file.rightsHolder" /></label>
                        <input type="text" id="rightsHolder" name="rightsHolder" value="${file.rightsHolder}" class="form-control">
                    </div>

                    <div class="form-group">
                        <label for="accessRights"><spring:message code="repository.file.accessRights" /></label>
                        <input type="text" id="accessRights" name="accessRights" value="${file.accessRights}" class="form-control">
                    </div>

                    <div class="form-group">
                        <label for="license"><spring:message code="repository.file.license" /></label>
                        <input type="text" id="license" name="license" value="${file.license}" class="form-control">
                    </div>
                    
                    <div class="form-group">
                        <label for="format"><spring:message code="repository.file.originalFilename" /></label>
                        <input type="text" id="originalFilename" name="originalFilename" value="${file.originalFilename}" class="form-control">
                    </div>

                    <div class="form-group">
                        <label for="format"><spring:message code="repository.file.contentType" /></label>
                        <input type="text" id="contentType" name="contentType" value="${file.contentType}" class="form-control">
                    </div>

                    <div class="form-group">
                        <label for="extent"><spring:message code="repository.file.extent" /></label>
                        <input type="text" id="extent" name="extent" value="${file.extent}" class="form-control">
                    </div>

                    <div class="form-group">
                        <label for="bibliographicCitation"><spring:message code="repository.file.bibliographicCitation" /></label>
                        <input type="text" id="bibliographicCitation" name="bibliographicCitation"
                               value="${file.bibliographicCitation}" class="form-control">
                    </div>

                    <div class="form-group">
                        <label for="dateSubmitted"><spring:message code="repository.file.dateSubmitted" /></label>
                        <input type="text" id="dateSubmitted" name="createdDate" value="${file.dateSubmitted}"
                               class="form-control">
                    </div>

                    <div class="form-group">
                        <label for="modified"><spring:message code="repository.file.lastModifiedDate" /></label>
                        <input type="text" id="modified" name="lastModifiedDate" value="${file.modified}" class="form-control">
                    </div>

                    <button type="submit" class="btn btn-default">Save</button>
                </form>
            </div>
        </div>
    </body>
</html>
