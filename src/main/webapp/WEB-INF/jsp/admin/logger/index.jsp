<!DOCTYPE html>

<%@ include file="/WEB-INF/jsp/init.jsp" %>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags" %>

<html>
<head>
<title><spring:message code="loggers.list.page" /></title>
</head>
<body>

	<table class="table table-striped">
		<thead>
			<tr>
				<th class="col-xs-8"><spring:message code="logger.name" /></th>
				<th class="col-xs-4 text-right"><spring:message
						code="logger.log-level" /></th>
			</tr>
		</thead>
		<tbody>
			<c:forEach items="${loggers.content}" var="logger" varStatus="status">
				<tr>
					<td class="col-xs-8 form-control-static"><a
						href="<c:url value="/admin/logger/${logger.name}." />"><c:out value="${logger.name}" /></a>
					</td>
					<td class="col-xs-4 text-right form-control-static"><c:out
							value="${logger.level}" /></td>
				</tr>
			</c:forEach>
		</tbody>
	</table>

	<div class="row">
		<form method="post" action=<c:url value="/admin/logger/addLoger"/>>
			<div class="form-group col-xs-8">
				<input class="form-control" type="text" name="nameNewLogger"
					placeholder="com.example.package.Class" />
			</div>
			<div class="form-group col-xs-4">
				<select class="form-control" name="loggerLevel">
					<c:forTokens items="all,debug,info,warn,error,fatal,off,trace"
						delims="," var="level">
						<option value="${level}" ${level == "debug" ? "selected" : ""}>
							<c:out value="${level}" /></option>
					</c:forTokens>
				</select>
			</div>
			<div class="form-group col-xs-2">
				<input type="submit" class="btn btn-primary"
					value="<spring:message code="logger.add-logger" />" />
			</div>

			<!-- CSRF protection -->
			<input type="hidden" name="${_csrf.parameterName}"
				value="${_csrf.token}" />
		</form>
	</div>
	
	
	<local:paginate2 page="${loggers}" />
	
	
</body>
</html>
