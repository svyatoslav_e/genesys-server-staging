<!DOCTYPE html>

<%@ include file="/WEB-INF/jsp/init.jsp" %>

<html>
<head>
    <title><spring:message code="oauth-client.page.list.title"/></title>
</head>
<body>
<h1>
    <spring:message code="oauth-client.list"/>
</h1>

<security:authorize access="hasRole('ADMINISTRATOR') or hasRole('VETTEDUSER')">
	<a href="<c:url value="/admin/oauth-clients/add-client" />" class="close"><spring:message code="add" /></a>
</security:authorize>

<table class="accessions">
    <tbody>
    <c:forEach items="${clientDetailsList}" var="clientDetail">
        <tr>
            <td>
                <a href="<c:url value="/admin/oauth-clients/${clientDetail.clientId}/"/>"><c:out
                        value="${clientDetail.title}"/></a>
            </td>
            <td><c:out value="${clientDetail.clientId}" /></td>
        </tr>
    </c:forEach>
    </tbody>
</table>
</body>
</html>
