<!DOCTYPE html>

<%@ include file="/WEB-INF/jsp/init.jsp" %>

<html>
<head>
<title><spring:message code="organization.page.profile.title" arguments="${organization.slug}" argumentSeparator="|" /></title>
</head>
<body>
	<h1>
		<c:out value="${organization.title}" />
		<small><c:out value="${organization.slug}" /></small>
	</h1>

	<div class="main-col-header">
		<a href="<c:url value="/org/${organization.slug}" />"><c:out value="${organization.slug}" /></a>
		<a href="<c:url value="/org/${organization.slug}/map" />"><spring:message code="maps.view-map" /></a>
		<a href="<c:url value="/org/${organization.slug}/data" />"><spring:message code="view.accessions" /></a>
	</div>
	
	<c:if test="${jsonInstitutes ne null}">
		<div class="row" style="">
		<div class="col-sm-12">
			<div id="map" class="gis-map gis-map-square"></div>
		</div>
		</div>

<content tag="javascript">		
		<script type="text/javascript">
		jQuery(document).ready(function() {	
			GenesysMaps.map("${pageContext.response.locale.language}", $("#map"), {
				maxZoom: 8,
				zoom: 2,
				center: new GenesysMaps.LatLng(0,0)
			}, function(el, map) {
				// markers
				var jsonInstitutes=${jsonInstitutes};
				jsonInstitutes.forEach(function(inst) {
					var marker = L.marker([inst.lat, inst.lng]).addTo(map);
					marker.bindPopup('<a href="<c:url value="/wiews/" />' + inst.code + '">' + inst.title + '</a>');
				});
				map.fitBounds(GenesysMaps.boundingBox(jsonInstitutes));
			});
		});
		</script>
</content>
	</c:if>
</body>
</html>