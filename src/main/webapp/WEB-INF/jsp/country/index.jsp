<!DOCTYPE html>

<%@ include file="/WEB-INF/jsp/init.jsp" %>

<html>
<head>
<title><spring:message code="country.page.list.title" /></title>
</head>
<body>
	<cms:informative-h1 title="country.page.list.title" fancy="true" info="country.page.list.intro" />

	<%-- <div class="main-col-header clearfix">
	<div class="nav-header pull-left">
		<div class="results">
			<spring:message code="paged.totalElements" arguments="${countries.size()}" />
		</div>
	</div>
	</div> --%>

	<div id="letter-top" class="main-col-header row">
	<div class="col-md-12 col-sm-12">
	<c:set value="" var="hoofdleter" />
	<c:forEach items="${countries}" var="country" varStatus="status">
		<c:if test="${hoofdleter ne country.getName(pageContext.response.locale).substring(0, 1)}">
			<c:set var="hoofdleter" value="${country.getName(pageContext.response.locale).substring(0, 1)}" />
			<a class="letter-pointer" href="#letter-${hoofdleter}"><c:out value="${hoofdleter}" /></a>
		</c:if>
	</c:forEach>
	</div>
	</div>


	<c:forEach items="${firstLetters}" var="hoofd">
		<div class="row">
			<div id="letter-B" class="hoofdleter"><c:out value="${hoofd}" />
				<small class="pull-right"><a href="#letter-top"><spring:message code="jump-to-top" /></a></small>
			</div>
			<ul class="funny-list list-unstyled">
				<c:forEach items="${countries}" var="country" varStatus="status">
					<c:if test="${country.getName(pageContext.response.locale).substring(0, 1) eq hoofd}">
						<li><a class="show ${not country.current ? 'disabled' : ''}" href="<c:url value="/geo/${country.code3}" />"><c:out value="${country.getName(pageContext.response.locale)}" /></a></li>
					</c:if>
				</c:forEach>
			</ul>
		</div>
	</c:forEach>
	
</body>
</html>