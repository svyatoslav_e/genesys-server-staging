<!DOCTYPE html>

<%@ include file="/WEB-INF/jsp/init.jsp" %>

<html>
<head>
<title><c:out value="${project.code} - ${project.name}" /></title>
<meta name="description" content="<c:out value="${jspHelper.htmlToText(blurp.summary)}" />" />
</head>
<body class="project-page text-page">
	<h1 class="green-bg">
		<c:out value="${project.name}" />
		<small>
			<c:out value="${project.code}" />
		</small>
	</h1>

	<security:authorize access="hasRole('ADMINISTRATOR') or hasPermission(#project, 'ADMINISTRATION')">
		<a href="<c:url value="/acl/${project.getClass().name}/${project.id}/permissions"><c:param name="back"><c:url value="/project/${project.code}" /></c:param></c:url>" class="close">
			<spring:message code="edit-acl" />
		</a>
	</security:authorize>
	<security:authorize access="hasRole('ADMINISTRATOR') or hasRole('CONTENTMANAGER') or hasPermission(#project, 'ADMINISTRATION')">
		<a href="<c:url value="/project/${project.code}/edit" />" class="close">
			<spring:message code="edit" />
		</a>
	</security:authorize>

	<!-- Project text -->
	<div class="row free-text-wrapper">
		<div class="collect-info clearfix project-description">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<cms:blurb blurb="${blurp}" />
			</div>
		</div>
			<c:if test="${project.url ne ''}">
				<div class="collect-info clearfix project-link">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<p><a title="<c:out value="${project.name}" />" href="<c:url value="${project.url}" />"><c:out value="${project.url}" /></a></p>
					</div>
				</div>
			</c:if>
	</div>

	<div class="collect-info general-info">
		<h4 class="row section-heading"><span><spring:message code="heading.general-info" /></span></h4>
		<div class="section-inner-content clearfix">
			<div class="">
				<div class="row institute-list">
					<div class="col-sm-12 col-xs-12 clearfix">
						<span class="stats-number">
							<fmt:formatNumber value="${countByProject}" />
						</span>
						<spring:message code="faoInstitutes.stat.accessionCount" />
					</div>
				</div>

				<c:if test="${countByProject gt 0}">
					<div class="buttons-wrapper">
						<a class="btn btn-primary" title="<spring:message code="faoInstitute.data-title" arguments="${project.name}" />" href="<c:url value="/project/${project.code}/data" />">
							<span class="glyphicon glyphicon-list"></span>
							<spring:message code="view.accessions" />
						</a>
						<a class="btn btn-default" title="<spring:message code="faoInstitute.overview-title" arguments="${project.name}" />" href="<c:url value="/project/${project.code}/overview" />">
							<span class="glyphicon glyphicon-eye-open"></span>
							<spring:message code="data-overview.short" />
						</a>
						<div class="btn-group">
							<form class="form-horizontal" method="post" action="<c:url value="/download/project/${project.code}/download" />">
								<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
								<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
									<span class="glyphicon glyphicon-download"></span>
									<span>
										<spring:message code="download" />
									</span>
									<span class="caret"></span>
								</button>
								<ul class="dropdown-menu">
									<security:authorize access="isAuthenticated()">
										<li>
											<button name="mcpd" class="btn btn-inline" type="submit">
												<spring:message code="filter.download-mcpd" />
											</button>
											<button name="pdci" class="btn btn-inline" type="submit">
												<spring:message code="filter.download-pdci" />
											</button>
										</li>
									</security:authorize>
									<li>
										<button name="dwca" class="btn btn-inline" type="submit">
											<spring:message code="metadata.download-dwca" />
										</button>
									</li>
								</ul>
							</form>
						</div>
					</div>
				</c:if>
			</div>
		</div>
	</div>

		<c:if test="${statisticsCrop ne null and statisticsCrop.totalCount gt 0}">
			<div class="collect-info stat-by-crop">
				<h4 class="row section-heading"><span><spring:message code="faoInstitute.stat-by-crop" /></span></h4>
					<div class="section-inner-content clearfix">
						<div class="">
							<local:term-result termResult="${statisticsCrop}" type="crop" />
						</div>
					</div>
			</div>
		</c:if>

		<c:if test="${statisticsGenus ne null and statisticsGenus.totalCount gt 0}">
			<div class="collect-info stat-by-genus">
				<h4 class="row section-heading"><span><spring:message code="faoInstitute.stat-by-genus" /></span></h4>
					<div class="section-inner-content clearfix">
						<div class="">
							<local:term-result termResult="${statisticsGenus}" type="genus" />
						</div>
					</div>
			</div>
		</c:if>

		<c:if test="${statisticsTaxonomy ne null and statisticsTaxonomy.totalCount gt 0}">
			<div class="collect-info stat-by-species">
				<h4 class="row section-heading"><span><spring:message code="faoInstitute.stat-by-species" /></span></h4>
					<div class="section-inner-content clearfix">
						<div class="">
							<local:term-result termResult="${statisticsTaxonomy}" type="species" />
						</div>
					</div>
			</div>
		</c:if>

		<c:if test="${statisticsOrigCty ne null and statisticsOrigCty.totalCount gt 0}">
			<div class="collect-info stat-by-country">
				<h4 class="row section-heading"><span><spring:message code="accession.orgCty" /></span></h4>
					<div class="section-inner-content clearfix">
						<div class="">
							<local:term-result termResult="${statisticsOrigCty}" type="country" />
						</div>
					</div>
				</div>
		</c:if>

	<c:if test="${fn:length(accessionLists) gt 0}">
		<div class="collect-info accessions-list">
			<h4 class="row section-heading"><span><spring:message code="project.accessionLists" /></span></h4>
			<div class="section-inner-content clearfix">
				<div class="">
					<ul>
						<c:forEach items="${accessionLists}" var="accessionList">
							<li>
								<a href="<c:url value="/explore"><c:param name="filter" value='{"lists":["${accessionList.uuid}"]}' /></c:url>">
									<c:out value="${accessionList.title}" />
								</a>
							</li>
						</c:forEach>
					</ul>
				</div>
			</div>
		</div>
	</c:if>

	<c:if test="${countByProject gt 0}">
		<div class="collect-info see-also">
			<h4 class="row section-heading"><span><spring:message code="heading.see-also" /></span></h4>
			<div class="section-inner-content clearfix">
				<div class="">
					<ul class="see-also">
						<li>
							<a href="<c:url value="/project/${project.code}/data/map" />">
								<spring:message code="see-also.map" />
							</a>
						</li>
						<li>
							<a href="<c:url value="/project/${project.code}/overview" />">
								<spring:message code="see-also.overview" />
							</a>
						</li>
						<c:if test="${project.url ne ''}">
						<li>
							<a title="<c:out value="${project.name}" />" href="<c:url value="${project.url}" />">
								<spring:message code="project.url" />
							</a>
						</li>
						</c:if>
					</ul>
				</div>
			</div>
		</div>
	</c:if>

</body>
</html>
