<?xml version="1.0" encoding="utf-8"?>
<%@ page contentType="application/atom+xml" pageEncoding="UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<feed xmlns="http://www.w3.org/2005/Atom"> 

	<title><spring:message code="page.home.title" /></title>
	<subtitle><spring:message code="activity.recent-activity" /></subtitle>
	<link href="<c:url value="${props.baseUrl}/content/news/feed.atom" />" rel="self" />
	<link href="<c:url value="${props.baseUrl}" />" />
	<id>urn:uuid:63851304-779e-4d9b-9485-3ea1c20592b1</id>
	<logo><c:url value="${props.baseUrl}/html/0/images/logo.svg" /></logo>
	<rights type="html"><spring:message code="footer.copyright-statement" /></rights>
	
<c:if test="${lastNews.size() gt 0}">
	<updated><fmt:formatDate value="${lastNews[0].lastModifiedDate}" pattern="yyyy-MM-dd'T'HH:mm:ss'Z'" /></updated>
</c:if>

<c:forEach items="${lastNews}" var="news" varStatus="status">
	<entry>
		<title><c:out value="${jspHelper.htmlToText(news.title)}" /></title>
		<link href="<c:url value="${props.baseUrl}/content/news/${news.id}/${jspHelper.suggestUrlForText(news.title)}" />" />
		<id><c:url value="${props.baseUrl}/content/news/${news.id}/#id" /></id>
		<published><fmt:formatDate value="${news.postDate.time}" pattern="yyyy-MM-dd'T'HH:mm:ss'Z'" /></published>
		<updated><fmt:formatDate value="${news.lastModifiedDate == null ? news.createdDate : news.lastModifiedDate}" pattern="yyyy-MM-dd'T'HH:mm:ss'Z'" /></updated>
		<%-- <summary><c:out value="${jspHelper.htmlToText(news.body, 400)}" /></summary> --%>

		<content type="xhtml">
			<div xmlns="http://www.w3.org/1999/xhtml">
				<c:out value="${news.body}" escapeXml="false" />
			</div>
		</content>
		<author>
<c:choose>
<c:when test="${news.lastModifiedBy ne null}">
			<name><c:out value="${jspHelper.userFullName(news.lastModifiedBy)}" /></name>
</c:when>
<c:when test="news.createdBy ne null">
			<name><c:out value="${jspHelper.userFullName(news.createdBy)}" /></name>
</c:when>
<c:otherwise>
			<name><spring:message code="page.home.title" /></name>
</c:otherwise>
</c:choose>
		</author>
	</entry>

</c:forEach>

</feed>
