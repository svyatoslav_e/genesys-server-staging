<!DOCTYPE html>

<%@ include file="init.jsp" %>

<html>
<head>
<title><spring:message code="page.home.title" /></title>
<style type="text/css">
.mcboatface { 
	margin: 1em 0 2em 2em;
	background-color: #f0f0f0;
	color: Black;
	padding: 0.5em; 
}
.mcboatface pre {
	margin: 1em;
}
hr.boaty {
	height: 10px;
	border-style: dotted;
	border-color: Black;
}
</style>
</head>
<body>

<%@ include file="lookandfeel/headers.jspf" %>
<hr class="boaty" />
<%@ include file="lookandfeel/alerts.jspf" %>
<hr class="boaty" />
<%@ include file="lookandfeel/freetext.jspf" %>
<hr class="boaty" />
<%@ include file="lookandfeel/forms.jspf" %>
<hr class="boaty" />


<content tag="javascript">
</content>
</body>
</html>