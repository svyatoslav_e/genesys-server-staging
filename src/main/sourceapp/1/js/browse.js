'use strict';

var BrowseUtil = {
    renderCropSuggestions: function (filters) {
        $('.radio-wrapper').remove();
        $.each(filters, function (name, value) {
            var input = $('<input/>', {
                type: 'radio',
                name: 'crops',
                id: 'crops_' + name,
                'i-key': 'crops',
                'class': 'filter-crop',
                value: name
            });
            if ($('div.filtval[x-key="crops' + name + '"]')[0] !== undefined) {
                input.prop('checked', true);
            }
            var key = Object.keys(this)[0];
            var label = $('<label/>', {
                'for': 'crops_' + name,
                text: key + ' (' + value[key] + ')'
            });
            var filtval = $('div.filtval[i-key^="crops"]');
            var attach = filtval[0] !== undefined ? filtval[0] : ('.crops button.applyBtn');
            var wrapper = $('<div/>', {'class': 'radio-wrapper'});
            wrapper.append(input);
            wrapper.append(label);
            $(attach).before(wrapper);
        });
    },

    renderListFilterSuggestions: function (filters, messages) {
        $.each(filters, function (option) {
            $('div.' + option).find('div[class!="panel-body"][class!="filtval complex"]').remove();
            if (filters[option].options[0] === undefined) {
                $('div.' + option).find('button.applyBtn').before('<div>No suggestions</div>');
            } else {
                $.each(filters[option].options, function (index) {
                    var div = '<div class="checkbox-wrapper">' +
                        '<label>' +
                        '<input class="filter-list"' +
                        'id="' + option + this.value + '_input"' +
                        ($('div.filtval[x-key="' + option + this.value + '"]')[0] !== undefined ? 'checked ' : '') +
                        'norm-key="' + option + '"' +
                        'i-key="' + option + '" type="checkbox"' +
                        'value="' + this.value + '"/>' +
                        messages[this.name] +
                        ' (' + filters[option].counts[index] + ')' +
                        '</label>' +
                        '</div>';
                    var filtval = $('div.filtval[x-key^="' + option + '"]');
                    var attach = filtval[0] !== undefined ? filtval[0] : $('div.' + option).find('button.applyBtn');
                    $(attach).before(div);
                });
            }
        });
    },

    renderBooleanSuggestions: function (filters) {
        $.each(filters, function (filter) {
            $.each($('input[type="checkbox"][i-key="' + filter + '"]'), function (index) {
                var val = filters[filter][index === 0 ? 'T' : 'F'];
                if (val === undefined) {
                    val = 0;
                }
                var text = $(this).parent().html();
                var idx = text.indexOf('(');
                if (idx !== -1) {
                    text = text.substring(0, idx);
                }
                $(this).parent().html(text + ' (' + val + ')');
                if ($('div[x-key="' + filter + $(this).val() + '"]')[0] !== undefined) {
                    $($('input[type="checkbox"][i-key="' + filter + '"]')[index]).prop('checked', true);
                }
            });
        });
    },

    applySuggestions: function (jsonData, messages) {
        $.ajax({
            url: '/explore/listFilterSuggestions',
            method: 'get',
            data: {
                filter: JSON.stringify(jsonData)
            },
            success: function (response) {
                BrowseUtil.renderListFilterSuggestions(response, messages);
            }
        });

        $.ajax({
            url: '/explore/booleanSuggestions',
            method: 'get',
            data: {
                filter: JSON.stringify(jsonData)
            },
            success: function (response) {
                BrowseUtil.renderBooleanSuggestions(response);
            }
        });

        $.ajax({
            url: '/explore/cropSuggestions',
            method: 'get',
            data: {
                filter: JSON.stringify(jsonData)
            },
            success: function (response) {
                BrowseUtil.renderCropSuggestions(response);
            }
        });
    },

    enableFilter: function (btn, jsonData) {
        var key = $(btn).attr('i-key');
        jsonData[key] = [];
    },

    cleanJsonData: function (jsonData) {
        $.each(jsonData, function (key) {
            if ($(this).length === 0) {
                delete jsonData[key];
            }
        });
    },

    i18nFilterMessage: {
        between: 'Between  ',
        varEnd: '  and  ',
        moreThan: 'More than  ',
        lessThan: 'Less than  ',
        like: 'Like  ',
        varTrue: 'Yes',
        varFalse: 'No',
        varNull: 'Unknown'
    }
};

document.addEventListener('DOMContentLoaded', function() {

    $(window).on('click', function (event) {
        var popup = document.getElementById('error-loading-popup-id');
        if(event.target === popup) {
            popup.style.display = 'none';
        }
    });

    $(document.getElementsByClassName('close')).on('click', function (event) {
        event.preventDefault();
        document.getElementById('error-loading-popup-id').style.display = 'none';
    });

    $('#collapseFilters').on('hidden.bs.collapse', function () {
        $('#collapseFilters').prev('.panel-heading').addClass('no-border');
    });

    $('#collapseFilters').on('hidden.bs.collapse', function () {
        $('#content-area').addClass('fullwidth');
    }).children().on('hidden.bs.collapse', function () {
        return false;
    });
    $('#collapseFilters').on('show.bs.collapse', function () {
        $('#content-area').removeClass('fullwidth');
    }).children().on('show.bs.collapse', function () {
        return true;
    });

    $('#collapseFilters').on('show.bs.collapse', function () {
        $('#collapseFilters').prev('.panel-heading').removeClass('no-border');
    });

});

