[[chApiAccession]]


== Managing Passport Data

Passport data is based on FAO Multi-Crop Passport Descriptors <<mcpd2>> format. 

Accession records are *upserted*, meaning that when the matching accession record 

. exists, it will be updated
. does not exist, a new record will be created

Accession data in the database will be updated with whatever data is provided in the 
request JSON.

=== Accession identity

Prior to full adoption of Permanent Unique Identifiers for Germplasm, accessions could be
identified by the holding institute code (INSTCODE) and the accession number (ACCENUMB).
Genebanks maintaining two or more collections of crops would sometimes use the same 
accession number, unique within one collection. 

Genesys uses the *instCode*, *acceNumb* and *genus* triplet to uniquely identify an 
accession in an institute:

[source,json,linenums]
----
{
	"instCode": "NGA039", <1>
	"acceNumb": "TMp-123", <2>
	"genus": "Musa" <3>
}
----
<1> Holding institute code (INSTCODE)
<2> Accession number (ACCENUMB)
<3> Genus (GENUS)


=== JSON data model

The JSON data model of accession passport data closely follows <<mcpd2, MCPD>> definitions.

By default, institutes in Genesys are configured to "Use unique accession numbers within the institute".
The accession JSON object must provide two identifying elements: `instCode` and `acceNumb`.

In cases where accession numbers are not unique within the institute, `genus` is used to identify
the unique accession within the institute. Then the Accession JSON object must always provide three
identifying elements: `instCode`, `acceNumb` and `genus`.

All other fields are optional.


[source,json,linenums]
----
{
	"instCode": "XYZ111",
	"acceNumb": "M12345",
	"cropName": "banana",
	"genus": "Musa",
	"species": "acuminata",
	"spauthor": "Colla",
	"subtaxa": "var. sumatrana",
	"subtauthor": "(Becc.) Nasution",
	"orgCty": ...,
	"acqDate": "20010301",
	"mlsStat": true,
	"inTrust": false,
	"available": true,
	"historic": false,
	"storage": [10, 20],
	"sampStat": 200,
	"duplSite": "BEL084",
	"bredCode": ...,
	"ancest": ....,
	"remarks": [ "remark1", "remark2" ],
	"acceUrl": "https://my-genebank.org/accession/1",
	"geo": {
		... <1>
	},
	"coll": {
		... <2>
	}
}
----
<1> JSON object with geographic data
<2> JSON object with collecting data

=== Clearing existing values

To reset or clear an existing value in the accession passport data, it should be provided
as `null`. Not providing a field means the field in the database should not be modified.

[source,json,linenums]
----
{
	"instCode": "NGA039",
	"acceNumb": "TMp-123",
	"genus": "Musa",
	"orgCty": null <1>
}
----
<1> Country of origin of accession is cleared by sending a `null` value.

===  Insert or update accessions

REST endpoint URL `/api/v0/acn/{instCode}/upsert` allows for inserting new accessions 
or updating existing records in Genesys. It accepts a JSON array of Accession JSON objects. 
The array provides for sending batches of 50 or 100 accessions in one call, reducing
the HTTP overhead and improving performance.

NOTE: Only the instCode and acceNumb are required (And in some cases genus).
NOTE: If a property is set to `null`, the existing value will be removed from the database.
NOTE: The server will return an error when `instCode` of JSONs does not match the `instCode` in the URL!


=== Deleting accessions

With the introduction of permanent identifiers for accession records in Genesys we have
also introduced the *Accession Archive*. The Archive holds passport data for accession records
that have been deleted from the active database.


REST endpoint URL `/api/v0/acn/{instCode}/delete` accepts an array of `instCode`, `acceNumb`, `genus` triplets
and deletes corresponding accession record from Genesys. The *DELETE* permission is required for this operation.

NOTE: Delete operation will fail if C&E data exists for any accessions listed.


.Delete 3 accessions from active database
[source,http,linenums]
----
POST /api/v0/acn/SYR002/delete

[{
	"instCode": "SYR002",
	"acceNumb": "12345",
	"genus": "Vicia"
}, {
	"instCode": "SYR002",
	"acceNumb": "12345",
	"genus": "Vicia"
}, {
	"instCode": "SYR002",
	"acceNumb": "IG 1",
	"genus": "Vicia"
}]
----



[bibliography]
- [[[mcpd2]]] Alercia, A; Diulgheroff, S; Mackay, M. 
	http://www.bioversityinternational.org/e-library/publications/detail/faobioversity-multi-crop-passport-descriptors-v2-mcpd-v2/[FAO/Bioversity Multi-Crop Passport Descriptors V.2]. 2012.

